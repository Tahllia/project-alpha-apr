**Feature 1**
*[x]Fork and clone the starter project from django-one-shot 
*[x]Create a new virtual environment in the repository directory for the project
    *[x]python -m  venv .venv
*[x]Activate the virtual environment
    *[x].\.venv\Scripts\Activate.ps1   
*[x]Upgrade pip
    *[x]python -m pip install --upgrade pip
*[x]Install django
    *[x]pip install django
*[x]Install black
    *[x]pip install black 
*[x]Install flake8
    *[x]pip install flake8
*[x]Install djlint
    *[x]pip install djlint
*[x]Install django debug toolbar
    *[x]python -m pip install django-debug-toolbar
        *[x]see this link: https://django-debug-toolbar.readthedocs.io/en/latest/installation.html 
*[x]Deactivate your virtual environment
    *[x]deactivate
*[x]Activate your virtual environment
    *[x].\.venv\Scripts\Activate.ps1
*[x]Use pip freeze to generate a requirements.txt file
    *[x]python -m pip freeze > requirements.txt

**Feature 2**
*[x]Create a Django project named tracker so that the manage.py file is in the top directory
    *[x]django-admin startproject tracker . 
*[x]Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
    *[x]python manage.py startapp accounts
*[x]Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
    *[x]python manage.py startapp projects 
*[x]Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
    *[x]python manage.py startapp tasks
*[x]Inside of tracker's settings.py under installed apps:
    *[x]"tasks.apps.TasksConfig",
    "accounts.apps.AccountsConfig",
    "projects.apps.ProjectsConfig",
    "debug_toolbar",
    *[x] Under MIDDLEWEARE: 
        *[x]MIDDLEWARE = [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
*[x]Inside of tracker's urls.py
    *[x]path('__debug__/', include('debug_toolbar.urls')),
*[]xRun the migrations
    *[x]python manage.py makemigrations
    *[x]python manage.py migrate
*[x]Create a super user
    *[x]python manage.py createsuperuser

**Feature 3**
*[x]Create a Project model in the projects Django app
    *model should have:
    *name-str-maxlength=200
    *decription-str-no max length 
    *memebers- many-to-many-Refers to the auth.User model, related name "projects"
**Feature 4**
*[x]Register the Project model with the admin so that you can see it in the Django admin site.

**Feature 5**
*[x]Create a view that will get all of the instances of the Project model and puts them in the context for the template
*[x]Register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
*[x]xInclude the URL patterns from the projects app in the tracker project with the prefix "projects/"
*[x]Create a template for the list view that complies with the following specifications from: https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/71-assessment-project.md
    *[x]Corrected mistake in the html where I called projectS_list instead of project_list for the if statment

**Feature 6**
*[x]In the tracker urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".
    *[x]path("", RedirectView.as_view(url=reverse_lazy("list_projects")),name="home",),
        *[x]remeber to NAME the path home as seen above

**Feature 7**
*[x]Register the LoginView  in your accounts urls.py with the path "login/" and the name "login"
*[x]Include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
*[x]Create a templates directory under accounts
*[x]Create a registration directory under templates
*[x]Create an HTML template named login.html in the registration directory
*[x]Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
*[X]In the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"

**Feature 8**
*[x]Protect the list view for the Project model so that only a person that has logged in can access it
*[x]Change the queryset of the view to filter the Project objects where members equals the logged in user

**Feature 9**
*[x]In the accounts/urls.py file
    *[x]Import the LogoutView from the same module that you imported the LoginView from
    *[x}Register that view in your urlpatterns list with the path "logout/" and the name "logout"
*[x]In the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page

**Feature 10**
*[x]Import the UserCreationForm from the built-in auth forms 
*[x]Use the special create_user  method to create a new user account from their username and password
*[x]Use the login  function that logs an account in
*[x]After you have created the user, redirect the browser to the path registered with the name "home"
*[x]Create an HTML template named signup.html in the registration directory
*[x]Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)

**Feature 11**
*[x]Create a Task model in the "tasks" Django app
*[x]The Task model should have the following attributes:
    *[x]name |   string  |   maximum length of 200 characters
    *[x]start_date   |   date-time
    *[x]due_date  |  date-time
    *[x]is_completed |   Boolean |   default value should be False
    *[x]project  | foreign key   |   Refers to the projects.Project model, related name "tasks", on delete cascade
    *[x]assignee |   foreign key |   Refers to the auth.User model, null is True, related name "tasks", on delete set null
*[x]The Task model should implicitly convert to a string with the __str__ method that is the value of the name property
*[x]Make migrations and migrate your database

**Feature 12**
*[x]Register the Task model with the admin so that you can see it in the Django admin site

**Feature 13**
*[x]Create a view that shows the details of a particular project
*[x]A user must be logged in to see the view
*[x]In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
*[x]Create a template to show the details of the project and a table of its tasks
*[x]Update the list template to show the number of tasks for a project
*[x]Update the list template to have a link from the project name to the detail view for that project

**Feature 14**
*This feature allows a person to go from the project list page to a page that allows them to create a new project.
*[x]Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
*[x]A person must be logged in to see the view
*[x]If the project is successfully created, it should redirect to the detail page for that project

*[x]Register that view for the path "create/" in the projects urls.py and the name "create_project"
*[x]Create an HTML template that shows the form to create a new Project (see the template specifications below)
*[x]Add a link to the list view for the Project that navigates to the new create view

**Feature 15**
*This feature is about creating a create view for the Task model, registering the view for a path, registering the tasks paths with the tracker project, and creating a template for the view.
*[x]Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
*[x]The view must only be accessible by people that are logged in
*[x]When the view successfully handles the form submission, have it redirect to the detail page of the task's project
*[x]Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
*[x]Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
*[x]Create a template for the create view that complies with the following specifications
*[x]Add a link to create a task from the project detail page that complies with the following specifications
*Template specifications
Two templates need either creation or modification.

Create form for Task
The resulting HTML from a request to the path http://localhost:8000/tasks/create/  should result in HTML that has

the fundamental five in it
a main tag that contains
div tag that contains
an h1 tag with the content "Create a new task"
a form tag with method "post" that contains any kind of HTML structures but must include
an input tag with type "text" and name "name"
an input tag with type "text" and name "start_date"
an input tag with type "text" and name "due_date"
a select tag with name "projects"
a select tag with name "assignee"
a button tag with the content "Create"


**Feature 16**
*[]Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
*[]The view must only be accessible by people that are logged in
*[]Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
*[]Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
Template specification
The resulting HTML from a request to the path http://localhost:8000/tasks/mine/  should result in HTML that has

the fundamental five in it
a main tag that contains
div tag that contains
an h1 tag with the content "My Tasks"
if there are tasks assigned to the person
a table with the headers "Name", "Start date", "End date", "Is completed" and a row for each task that is assigned to the logged in person
In the last column, if the task is completed, it should show the word "Done", otherwise, it should show nothing
otherwise
a p tag with the content "You have no tasks"

**Feature 17**
*This feature allows a person to update the status on one of their assigned tasks from incomplete to complete.
*[x]Create an update view for the Task model that only is concerned with the is_completed field
*[x]When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
*[x]Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
*[]No need to make a template for this view
*[x]Modify the "My Tasks" view to comply with the template specification
*Template Specificication
*[x]<form method="post" action="{% url 'complete_task' task.id %}">
  {% csrf_token %}
  <input type="hidden" name="is_completed" value="True">
  <button>Complete</button>
</form>
    *[x]When the person clicks that button, the task's is_completed status should be updated to True and, then, the form disappears

**Feature 18**
*[x]Install the django-markdownify package using pip (the first pip command in the instructions) and put it into the INSTALLED_APPS in the tracker settings.py according to the Installation  instructions block
    *[x]I used the method of directly updating my requirment's text and running pip install -r requirements.txt
    *[x]Add markdownify to settings.py 
    *[x]INSTALLED_APPS = [
    ...
    'markdownify.apps.MarkdownifyConfig',]
*[x]Also, in the tracker settings.py file, add the configuration setting to disable sanitation 
    *[]MARKDOWNIFY = {
    "default": {
        "BLEACH": False}}
*[x]In your the template for the Project detail view, load the markdownify template library as shown in the Usage  section
*[x]Replace the p tag and {{ project.description }} in the Project detail view with this code
    *[x]{{ project.description|markdownify }}
    *[x]NOTE: I actually had to use:  {{project_detail.description|markdownify}} in order to get it to pass the test
*[]Use pip freeze to update your requirements.txt file
    *[]I had already updated my requirments.txt maually, but I did this step just in case

**Feature 19**
*In this feature, you'll add some navigation to the Web application. Hopefully, you have used template inheritance so that you can just do this in one place. Otherwise, you'll need to add the HTML to all of the places.

*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
